import xlrd
import xlwt
from xlwt import Workbook
import numpy as np
import scipy as sc


def getResults():
    us_results_file = open('Emotive_Analysis/LIWC_US_Tweet_Results.txt', 'r', encoding='utf-8')
    us_result_lines = us_results_file.readlines()
    us_results = []
    india_results_file = open(
        'Emotive_Analysis/LIWC_India_Tweet_Results.txt', 'r', encoding='utf-8')
    india_result_lines = india_results_file.readlines()
    india_results = []

    # Gets all the data for a tweet into an array. Category is index [2] followed by LIWC data
    for line in us_result_lines:
        us_results.append(line.split("\t"))
    for line in india_result_lines:
        india_results.append(line.split("\t"))
    liwc_categories = us_results[0]

    return us_results, india_results, liwc_categories


def getRelationshipBins():
    # This prepares the categories and their bins
    bin_words = xlrd.open_workbook('relationship_bins/bin_words.xlsx').sheet_by_index(0)
    categories = bin_words.row_values(0)
    relation_words_dict = {}

    for i, relation in enumerate(categories):
        relation_words_dict[relation] = bin_words.col_values(i, 1)

    return relation_words_dict


def main():
    np.set_printoptions(suppress=True)

    us_results, india_results, liwc_categories = getResults()
    relation_words_dict = getRelationshipBins()

    # Sort the data into their categories
    us_data = {}
    us_composite = {}
    india_data = {}
    india_composite = {}
    for bucket in relation_words_dict:
        us_data[bucket] = []
        us_composite[bucket] = []
        india_data[bucket] = []
        india_composite[bucket] = []
        for i in range(len(us_results[0])):
            us_composite[bucket].append(0)
            india_composite[bucket].append(0)

    # Put all the data into a dictionary and also composite it for averaging
    i = 0
    for tweet in us_results:
        found = False
        for bucket in relation_words_dict:
            if tweet[0].lower() in relation_words_dict[bucket]:
                us_data[bucket].append(tweet)
                for i in range(len(tweet) - 1):
                    us_composite[bucket][i + 1] += float(tweet[i + 1])
                found = True
                break
        if not found:
            print(tweet[0] + " not found in any bucket")
        i += 1
    for tweet in india_results:
        # the 'tweet' here is the LIWC result stored in an array, not the actual tweet
        found = False
        for bucket in relation_words_dict:
            if tweet[0].lower() in relation_words_dict[bucket]:
                india_data[bucket].append(tweet)
                # in this for loop we go through and add each value from our LIWC result
                # to the corresponding total in the composite dictionary
                for i in range(len(tweet) - 1):
                    india_composite[bucket][i + 1] += float(tweet[i + 1])
                found = True
                break
        if not found:
            print(tweet[0] + " not found in any bucket")

    # Get Averages and medians
    # and counts

    # Counts
    us_counts = {}
    india_counts = {}
    for bucket in relation_words_dict:
        us_counts[bucket] = []
        india_counts[bucket] = []
        # for data in us_data[bucket]:
        #
        # for i in range(len(us_results[0])):
        #     us_counts[bucket].append()

    # Averages
    us_averages = {}
    india_averages = {}
    for bucket in us_composite:
        numUSTweets = len(us_data[bucket])
        numIndiaTweets = len(india_data[bucket])
        print(bucket + " numUSTweets: " + str(numUSTweets))
        print(bucket + " numIndiaTweets: " + str(numIndiaTweets))
        us_averages[bucket] = np.array(us_composite[bucket]) / numUSTweets
        india_averages[bucket] = np.array(india_composite[bucket]) / numIndiaTweets

    # Medians: convert to numpy array and get medians by column
    us_medians = {}
    india_medians = {}
    for bucket in us_data:
        us_array = np.array(us_data[bucket])
        us_array = us_array[:, 1:].astype(np.float)
        india_array = np.array(india_data[bucket])
        india_array = india_array[:, 1:].astype(np.float)
        us_medians[bucket] = np.median(us_array, axis=0)
        india_medians[bucket] = np.median(india_array, axis=0)

    # Output Medians
    wb = Workbook()
    us_sheet = wb.add_sheet('US Medians')
    india_sheet = wb.add_sheet('India Medians')

    row = 0
    for bucket in us_composite:
        col = 0
        us_sheet.write(row, col, bucket)
        india_sheet.write(row, col, bucket)
        col += 1
        i = 0
        for value in us_medians[bucket]:
            us_sheet.write(row, col, liwc_categories[i])
            us_sheet.write(row+1, col, value)
            india_sheet.write(row, col, liwc_categories[i])
            india_sheet.write(row+1, col, india_medians[bucket][i])
            col += 1
            i += 1
        row += 2

    wb.save('Emotive_Analysis/LIWC_Medians0.xls')

    # Output Averages
    wb = Workbook()
    us_sheet = wb.add_sheet('US Averages')
    india_sheet = wb.add_sheet('India Averages')

    row = 0
    for bucket in us_composite:
        col = 0
        us_sheet.write(row, col, bucket)
        india_sheet.write(row, col, bucket)
        col += 1
        i = 0
        for value in us_averages[bucket]:
            us_sheet.write(row, col, liwc_categories[i])
            us_sheet.write(row+1, col, value)
            india_sheet.write(row, col, liwc_categories[i])
            india_sheet.write(row+1, col, india_averages[bucket][i])
            col += 1
            i += 1
        row += 2

    wb.save('Emotive_Analysis/LIWC_Averages0.xls')
