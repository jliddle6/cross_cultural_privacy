import xlrd
import numpy as np
import scipy.stats as sc
import matplotlib.pyplot as plt
# import sys
# import liwc_results_analysis
# sys.path.append('Cross_Cultural_Privacy/Emotive_Analysis')


def getResults():
    us_results_file = open('Emotive_Analysis/LIWC_US_Tweet_Results.txt', 'r', encoding='utf-8')
    us_result_lines = us_results_file.readlines()
    us_results = []
    india_results_file = open(
        'Emotive_Analysis/LIWC_India_Tweet_Results.txt', 'r', encoding='utf-8')
    india_result_lines = india_results_file.readlines()
    india_results = []

    # Gets all the data for a tweet into an array. Category is index [2] followed by LIWC data
    for line in us_result_lines:
        us_results.append(line.split("\t"))
    for line in india_result_lines:
        india_results.append(line.split("\t"))
    liwc_categories = us_results[0]

    return us_results, india_results


def getRelationshipBins():
    # This prepares the categories and their bins
    bin_words = xlrd.open_workbook('relationship_bins/bin_words.xlsx').sheet_by_index(0)
    categories = bin_words.row_values(0)
    relation_words_dict = {}

    for i, relation in enumerate(categories):
        relation_words_dict[relation] = bin_words.col_values(i, 1)

    return relation_words_dict


global us_results, india_results
us_results, india_results = getResults()
global relation_words_dict
relation_words_dict = getRelationshipBins()


def getLIWCValueByIndex(index):
    global us_results, india_results, relation_words_dict

    us_dict = {}
    india_dict = {}
    for bucket in relation_words_dict:
        us_dict[bucket] = []
        india_dict[bucket] = []

    # for each of our tweet results
    for tweet in us_results:
        for bucket in relation_words_dict:
            # make sure that we're putting it in the right relationship
            if tweet[0].lower() in relation_words_dict[bucket]:
                us_dict[bucket].append(tweet[index])
                # don't keep looking for it if we've found it
                break

    for tweet in india_results:
        for bucket in relation_words_dict:
            if tweet[0].lower() in relation_words_dict[bucket]:
                india_dict[bucket].append(tweet[index])
                break

    return us_dict, india_dict


def printStats(us_dict, india_dict, category):
    # now find the stuff we want for each bucket
    print("US %s:" % category)
    for bucket in us_dict:
        print(bucket)
        print("Count: %d" % len(us_dict[bucket]))
        print("Average: %f" % np.average(np.array(us_dict[bucket]).astype(np.float)))
        print("St. Dev: %f" % np.std(np.array(us_dict[bucket]).astype(np.float)))

    print()
    print()
    print()

    print("India %s:" % category)
    for bucket in india_dict:
        print(bucket)
        print("Count: %d" % len(india_dict[bucket]))
        print("Average: %f" % np.average(np.array(india_dict[bucket]).astype(np.float)))
        print("St. Dev: %f" % np.std(np.array(india_dict[bucket]).astype(np.float)))

    print()
    print()
    print()


def printIndTTest(us_dict, india_dict):
    # Now let's run some ttests
    for bucket in us_dict:
        print(bucket)
        print(sc.ttest_ind(np.array(us_dict[bucket]).astype(
            np.float), np.array(india_dict[bucket]).astype(np.float)))


def getEmotionCounts(pos_list, neg_list):
    # pass in a list for a relationship rather than a whole dictionary
    # basically what I want to do is iterate through them together, and find if they're pos, neg, both, or neither
    pos = 0
    neg = 0
    both = 0
    neither = 0

    total = len(pos_list)

    # they should be the same length
    for i in range(0, total):
        pos_val = pos_list[i]
        neg_val = neg_list[i]
        is_pos = float(pos_val) > 0
        is_neg = float(neg_val) > 0

        if is_pos and is_neg:
            both += 1
        elif is_pos and not is_neg:
            pos += 1
        elif not is_pos and is_neg:
            neg += 1
        elif not is_pos and not is_neg:
            neither += 1

    # normalize?
    pos = pos / total
    neg = neg / total
    both = both / total
    neither = neither / total

    print("Num Pos: %f" % pos)
    print("Num Neg: %f" % neg)
    print("Num Both: %f" % both)
    print("Num Neither: %f" % neither)


def getMoreEmotionCounts(pos_list, neg_list):
    # In this function, both means they're equal.
    pos = 0
    neg = 0
    both = 0
    neither = 0

    total = len(pos_list)

    for i in range(0, total):
        pos_val = float(pos_list[i])
        neg_val = float(neg_list[i])

        if pos_val > neg_val:
            pos += 1
        elif pos_val < neg_val:
            neg += 1
        elif pos_val == neg_val:
            if pos_val == 0:
                neither += 1
            else:
                both += 1

    # normalize?
    pos = pos / total
    neg = neg / total
    both = both / total
    neither = neither / total

    print("Num More Pos: %f" % pos)
    print("Num More Neg: %f" % neg)
    print("Num Equal: %f" % both)
    print("Num Neither: %f" % neither)

    return [pos, neg, both, neither]


def printPieChart(labels, data, title):
    # Pie chart, where the slices will be ordered and plotted counter-clockwise:
    labels.reverse()
    data.reverse()
    fig1, ax1 = plt.subplots()
    ax1.pie(data, labels=labels, autopct='%1.1f%%', startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ax1.set_title(title)


us_posemo_dict, india_posemo_dict = getLIWCValueByIndex(19)
# printStats(us_posemo_dict, india_posemo_dict, "Posemo")
# print("Posemo: \n")
# printIndTTest(us_posemo_dict, india_posemo_dict)
# print()

us_negemo_dict, india_negemo_dict = getLIWCValueByIndex(20)
# printStats(us_negemo_dict, india_negemo_dict, "Negemo")
# print("Negemo: \n")
# printIndTTest(us_negemo_dict, india_negemo_dict)
# print()

for bucket in relation_words_dict:
    print(bucket)
    print()
    print("US")
    # getEmotionCounts(us_posemo_dict[bucket], us_negemo_dict[bucket])
    data = getMoreEmotionCounts(us_posemo_dict[bucket], us_negemo_dict[bucket])
    title = "US " + bucket
    printPieChart(["Pos", "Neg", "Equal", "Neither"], data, title)

    print()
    print("India")
    # getEmotionCounts(india_posemo_dict[bucket], india_negemo_dict[bucket])
    data = getMoreEmotionCounts(india_posemo_dict[bucket], india_negemo_dict[bucket])
    title = "India " + bucket
    printPieChart(["Pos", "Neg", "Equal", "Neither"], data, title)

    print()
    print()

plt.show()

# TODO t-tests for counts
# In order to work with t-tests on counts, maybe have the 4 separate arrays with
# 0s or 1s. We'll have to check each one against each other one, but it could be worth a shot?

# RESULTS: On the posemo side we have sibling, grandparents (alpha of .05), and possibly indirect fam (alpha of .1)
# On the negemo side we have possibly sig other, sibling, and parent (alpha of .1)

# I think that the getMoreEmotionCounts is more helpful, some of the tweets I've seen
# have both but really should only have one. The tricky one is when it's even and it's not accurate
