import dragn_client as dc
from scipy import spatial
import xlrd
# import os
#
# os.chdir("/Users/jacob/Cross_Cultural_Privacy/liwc_analysis")
#
# with open('LIWC_Analysis/posemo_words.txt', 'r', encoding='utf8') as p:
posemo_words = []
with open('word_groups/posemo_words.txt', 'r') as p:
    words = p.read()
    posemo_words = words.split()

# With this I can set something up to put all the tweets into a list,
# then split them as individual tweets, then run the analysis below

# book = xlrd.open_workbook('data/RelationshipTweets_LabeledClipped.xlsx')
book = xlrd.open_workbook('data/Test_Tweets.xlsx')
us = book.sheet_by_index(0)
# india = book.sheet_by_index(1)
tweets = us.col_values(1, 1)

split_tweets = []
for tweet in tweets:
    split_tweets.append(tweet.split())

# split_tweets.append("I #love my wonderful partner he's so amazing and kinda sweet.".split())

c = dc.Client()

# c.get_docs('embeddings', 'get_vectors')

# I'm going to store the words to be removed so that I'm not removing them inside the for loop
words_to_remove = []
tweet_vectors = []
for tweet_words in split_tweets:
    tweet_vector = []
    for word in tweet_words:
        try:
            tweet_vector.append(c.run("embeddings", "get_vectors", [word], space='glove')[0])
        except:
            words_to_remove.append(word)
            continue
    tweet_vectors.append(tweet_vector)

# Here I'll remove all the words that had issues
for word in words_to_remove:
    if word in tweet_words:
        tweet_words.remove(word)

words_to_remove = []
pos_vectors = []
for word in posemo_words:
    try:
        pos_vectors.append(c.run("embeddings", "get_vectors", [word], space='glove')[0])
    except:
        words_to_remove.append(word)
        continue

for word in words_to_remove:
    if word in posemo_words:
        posemo_words.remove(word)

replacement_threshold = .1
new_tweets = []

for tweet_vector, tweet_words in zip(tweet_vectors, split_tweets):
    new_tweet_words = []

    for vector, word in zip(tweet_vector, tweet_words):
        new_word = word

        if not word in posemo_words:
            for pos_vector, pos_word in zip(pos_vectors, posemo_words):
                dist = spatial.distance.cosine(vector, pos_vector)

                if dist < replacement_threshold:
                    print(word + " " + pos_word + " " + str(dist))
                    new_word = pos_word
                    break

        new_tweet_words.append(new_word)

    new_tweets.append(" ".join(new_tweet_words))

print(new_tweets)
