import numpy as np

glove_dictionary = {}
word_list = []
glove_vectors = []

with open('glove_vectors.txt', 'r', encoding='utf8') as f:
    data = f.read()
    lines = data.split('\n')
    for line in lines:
        tokens = line.split(' ')
        word_list.append(tokens[0])
        glove_vectors.append(np.array([float(t) for t in tokens[1:]]))
        glove_dictionary[tokens[0]] = np.array([float(t) for t in tokens[1:]])
