# using the server is fairly simple. If you've already got dragon_client.py, and are either
#  ssh'ed into the BYU cs department or are on the BYU CS VPN
#  (or maybe just being on campus works? I'm not sure yet!)

# Set up a client
import dragn_client as dc
c = dc.Client()

# Get some vectors
# The first time an embedding space is used it may take a while to load unless someone else has already loaded it recently, and then responses will be snappy after that
# GloVe is actually default so that parameter can be removed
c.run("embeddings", "get_vectors", ["example", "cat", "dog",
                                    "mouse", "random", "words", "go", "here"], space='glove')

# To get more info about which functions are available on this server module and how to use them, use:
c.list_functions("embeddings")
# c.get_docs("embeddings", "function_name_goes_here")

# For more documentation, go here: https://bitbucket.org/nibbetts/dragn-server/src/master/
