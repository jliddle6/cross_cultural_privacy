import xlrd

book = xlrd.open_workbook("LIWC2015_words.xlsx")
sheet = book.sheet_by_index(0)

posemo_words = []
negemo_words = []
anx_words = []
anger_words = []
sad_words = []

for i in range(sheet.nrows):
    if sheet.cell_value(i, 12) != 0:
        posemo_words.append(sheet.cell_value(i, 0))
    if sheet.cell_value(i, 13) != 0:
        negemo_words.append(sheet.cell_value(i, 0))
    if sheet.cell_value(i, 14) != 0:
        anx_words.append(sheet.cell_value(i, 0))
    if sheet.cell_value(i, 15) != 0:
        anger_words.append(sheet.cell_value(i, 0))
    if sheet.cell_value(i, 16) != 0:
        sad_words.append(sheet.cell_value(i, 0))

pos = open("posemo_words.txt", "w")
neg = open("negemo_words.txt", "w")
anx = open("anx_words.txt", "w")
ang = open("anger_words.txt", "w")
sad = open("sad_words.txt", "w")

for word in posemo_words:
    pos.write(word)
    pos.write('\n')
for word in negemo_words:
    neg.write(word)
    neg.write('\n')
for word in anx_words:
    anx.write(word)
    anx.write('\n')
for word in anger_words:
    ang.write(word)
    ang.write('\n')
for word in sad_words:
    sad.write(word)
    sad.write('\n')

pos.close()
neg.close()
anx.close()
ang.close()
sad.close()
