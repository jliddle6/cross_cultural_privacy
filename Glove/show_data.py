import matplotlib.pyplot as plt
import numpy as np
import xlrd

# The next few lines set up our categories so that we can take any relationship and
# get the category for that relationship.
bin_words = xlrd.open_workbook('word_groups/bin_words.xlsx').sheet_by_index(0)

categories = bin_words.row_values(0)

relation_words_dict = {}
i = 0
for relation in categories:
    relation_words_dict[relation] = bin_words.col_values(i, 1)
    i += 1

# We'll store our emotion data in several dictionaries initialized here
percent_positive = {}
percent_negative = {}
percent_angry = {}
percent_anx = {}
percent_sad = {}
for category in categories:
    percent_positive[category] = []
    percent_negative[category] = []
    percent_angry[category] = []
    percent_anx[category] = []
    percent_sad[category] = []

# Now we need to get the data from LIWC results, put them into their relationship
# categories, and find out if they're pos, neg, or some mix.
RELATIONSHIP_LOC = 2

with open('word_groups/results.txt', 'r') as f:
    data = f.read()
    lines = data.split('\n')
    for line in lines:
        tokens = line.split(' ')
        relationship = tokens[RELATIONSHIP_LOC]
        for bucket in relation_words_dict:
            if not relationship.lower() in relation_words_dict[bucket]:
                continue
            else:
                relationship = bucket
