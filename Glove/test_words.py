import dragn_client as dc
from scipy import spatial


posemo_words = []
with open('word_groups/posemo_words.txt', 'r') as p:
    words = p.read()
    posemo_words = words.split()

c = dc.Client()

words_to_remove = []
pos_vectors = []
for word in posemo_words:
    try:
        pos_vectors.append(c.run("embeddings", "get_vectors", [word], space='glove')[0])
    except:
        words_to_remove.append(word)
        continue

for word in words_to_remove:
    if word in posemo_words:
        posemo_words.remove(word)

test_words = ['#Love']
words_to_check = posemo_words
# distractors = ['love','hate','annoyed','citizen','person','penguin','artichoke','platypus','pontificate','abstract','beautiful','ugly']
relationship_vectors = dc.run("embeddings", "get_vectors", test_words)
comparison_vectors = pos_vectors  # dc.run("embeddings","get_vectors",words_to_check)
combined_words = words_to_check
for i, v in enumerate(relationship_vectors):
    print()
    print(test_words[i])
    distances = []
    for j, c in enumerate(comparison_vectors):
        distance = spatial.distance.cosine(c, v)
        distances.append(distance)
    sorted_distances = sorted(zip(distances, combined_words))
    for pair in sorted_distances:
        print('    ', pair[1], '    ', pair[0])
