import winsound
import spacy
from spacy import displacy
from pathlib import Path
import xlrd
import xlwt
from xlutils.copy import copy
import os

for file in os.listdir("Excel_Input"):

    book = xlrd.open_workbook("Excel_Input/" + file)
    sheet = book.sheet_by_index(0)
    # print(advisor.cell_value(2, 1))

    tweetsStrings = sheet.col_values(1, 1)

    nlp = spacy.load("en_core_web_md")  # use larger model for better accuracy
    tweets = []
    i = 0
    for tweet in tweetsStrings:
        tweets.append(nlp(tweet))
        i += 1

    writeBook = copy(book)
    sheetOut = writeBook.get_sheet(0)

    possessiveTuple = ("my", "our")  # What to do with me?
    relationTuple = ("advisors", "ancestors", "associates", "aunts", "aunties", "auntys",
                     "babes", "baby", "better half", "bffs",  "boss", "boyfriends", "brides",
                     "bridegrooms", "bros", "brothers", "brotherly", "buddy", "child",
                     "children", "chums", "clans", "classmates", "cohorts", "colleagues",
                     "consorts", "cousins", "coworkers", "crony", "cronies", "crush", "dads",
                     "daddys", "darlings", "daughters", "dearests", "descendants", "elders",
                     "fams", "family", "families", "fathers", "fiancees", "fiancées", "fiances",
                     "fiancés", "folks", "friends", "girlfriends", "grandchild", "grandchildren",
                     "grandaughters", "grandfathers", "grandmas", "grandmothers", "grandpas",
                     "grandparents", "grandsons", "granny", "guardian", "guru", "honeys", "hubbys",
                     "huns", "husbands", "in-laws", "in laws", "infants", "juniors", "kids", "kins",
                     "kindreds", "kinship", "kith", "loves", "lovers", "mas", "mamas", "managers",
                     "masters", "mates", "maternal", "mentees", "mentors", "moms", "mommys", "mums",
                     "mothers", "mummys", "nephews", "nieces", "offsprings", "pas", "pals", "papas",
                     "parents", "partners", "paternal", "progenys", "relatives", "roomies",
                     "roommates", "schoolmates", "seniors", "sibs", "siblings", "sidekicks", "sis",
                     "sisters", "sisterhoods", "sisterly", "sons", "soul mates", "soulmates",
                     "spouses", "stepdads", "stepmoms", "stepsons", "stepdaughters", "stepsisters",
                     "stepbrothers", "stepmothers", "stepfathers", "subordinates", "superiors",
                     "supervisors", "sweethearts", "sweetys", "teachers", "teammates", "toddlers",
                     "twins", "uncles", "wifes", "wifeys")
    i = 1
    for tweet in tweets:
        # print(tweet)
        for word in tweet:
            if word.text.lower() in possessiveTuple:
                # if word.dep_ == "poss":  # and word.text != "me":
                validHead = False
                for relationWord in relationTuple:
                    if str(word.head).lower() in relationWord:
                        validHead = True
                        break
                if validHead:
                    sheetOut.write(i, 7, "Valid")
                    print("Valid")
        i += 1

    writeBook.save("Excel_Output/" + file[:-1])

duration = 250  # milliseconds
freq = 440  # Hz
winsound.Beep(freq, duration)
