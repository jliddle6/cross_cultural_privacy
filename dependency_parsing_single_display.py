import spacy
from spacy import displacy
from pathlib import Path

nlp = spacy.load("en_core_web_md")  # use larger model for better accuracy

doc = nlp("Mom and my two aunts are newly added judge of Nepal Idol. ")

svg = displacy.render(doc, style="dep", options={"compact": True})
output_path = Path("/Users/jacob/Cross_Cultural_Privacy/aunt4.svg")
output_path.open("w").write(svg)

for token in doc:
    print(token.text,  token.pos_, token.tag_, token.dep_, token.head)
