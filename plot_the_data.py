import matplotlib.pyplot as plt
import numpy as np
import xlrd

categories = []


def getData():
    book = xlrd.open_workbook("RelationshipTweets_Labeled.xlsx")
    india = book.sheet_by_index(0)
    us = book.sheet_by_index(2)

    india_tweets = india.col_values(1, 1)
    india_relationships = india.col_values(2, 1)
    india_replies = india.col_values(5, 1)
    india_retweets = india.col_values(6, 1)
    india_likes = india.col_values(7, 1)
    us_tweets = us.col_values(1, 1)
    us_relationships = us.col_values(2, 1)
    us_replies = us.col_values(5, 1)
    us_retweets = us.col_values(6, 1)
    us_likes = us.col_values(7, 1)

    return [india_tweets, india_relationships, india_replies, india_retweets, india_likes,
            us_tweets, us_relationships, us_replies, us_retweets, us_likes]


def getDicts():
    # The next few lines set up our categories so that we can take any relationship and
    # get the category for that relationship.
    bin_words = xlrd.open_workbook('relationship_bins/bin_words.xlsx').sheet_by_index(0)

    global categories
    categories = bin_words.row_values(0)

    relation_words_dict = {}
    i = 0
    for relation in categories:
        relation_words_dict[relation] = bin_words.col_values(i, 1)
        i += 1

    # sig_other = bin_words.col_values(0, 1)
    # sibling = bin_words.col_values(1, 1)
    # parent = bin_words.col_values(2, 1)
    # relative = bin_words.col_values(3, 1)
    # child = bin_words.col_values(4, 1)
    # friend = bin_words.col_values(5, 1)
    # associate = bin_words.col_values(6, 1)
    # subordinate = bin_words.col_values(7, 1)
    # superior = bin_words.col_values(8, 1)

    india_replies_dict = {}
    india_retweets_dict = {}
    india_likes_dict = {}
    india_tweets_dict = {}
    us_replies_dict = {}
    us_retweets_dict = {}
    us_likes_dict = {}
    us_tweets_dict = {}
    for r in categories:
        india_replies_dict[r] = []
        india_retweets_dict[r] = []
        india_likes_dict[r] = []
        india_tweets_dict[r] = []
        us_replies_dict[r] = []
        us_retweets_dict[r] = []
        us_likes_dict[r] = []
        us_tweets_dict[r] = []

    data = getData()

    india_tweets = data[0]
    india_relationships = data[1]
    india_replies = data[2]
    india_retweets = data[3]
    india_likes = data[4]
    us_tweets = data[5]
    us_relationships = data[6]
    us_replies = data[7]
    us_retweets = data[8]
    us_likes = data[9]

    # assign the relationship for each tweet to the appropriate bucket and fill the dictionaries
    i = 0
    for relationship in india_relationships:
        found = False
        for bucket in relation_words_dict:
            if relationship.lower() in relation_words_dict[bucket]:
                relationship = bucket
                india_replies_dict[relationship].append(int(india_replies[i]))
                india_retweets_dict[relationship].append(int(india_retweets[i]))
                india_likes_dict[relationship].append(int(india_likes[i]))
                india_tweets_dict[relationship].append(india_tweets[i])
                found = True
                break
        if not found:
            print(relationship + " not found in any bucket")
        i += 1

    i = 0
    for relationship in us_relationships:
        found = False
        for bucket in relation_words_dict:
            if relationship.lower() in relation_words_dict[bucket]:
                relationship = bucket
                us_replies_dict[relationship].append(int(us_replies[i]))
                us_retweets_dict[relationship].append(int(us_retweets[i]))
                us_likes_dict[relationship].append(int(us_likes[i]))
                us_tweets_dict[relationship].append(us_tweets[i])
                found = True
                break
        if not found:
            print(relationship + " not found in any bucket")
        i += 1

    return [india_replies_dict, us_replies_dict, india_retweets_dict, us_retweets_dict,
            india_likes_dict, us_likes_dict, india_tweets_dict, us_tweets_dict]


def boxSideBySide(dict1, dict2, ylabel, title1, title2):

    data1 = list(dict1.values())
    data2 = list(dict2.values())
    # for bin in dict1:
    #     data1.append(dict1[bin])
    #     data2.append(dict2[bin])

    fig, axes = plt.subplots(ncols=2, constrained_layout=True)
    ax1, ax2 = axes.flatten()
    ax1.set_xlabel('Relationship')
    ax1.set_ylabel(ylabel)
    ax1.set_title(title1)
    ax1.set_xticklabels(categories, rotation=45, ha='right')
    ax2.set_xlabel('Relationship')
    ax2.set_ylabel(ylabel)
    ax2.set_title(title2)
    ax2.set_xticklabels(categories, rotation=45, ha='right')

    bd1 = ax1.boxplot(data1)
    bd2 = ax2.boxplot(data2)

    # find the medians to dynamically sort the boxplot by median
    med1 = []
    med2 = []
    max1 = []
    max2 = []
    catSort1 = categories
    catSort2 = categories
    for med in bd1['medians']:
        med1.append(med.get_ydata()[0])
    for med in bd2['medians']:
        med2.append(med.get_ydata()[0])
    for index, max in enumerate(bd1['caps']):
        if index % 2 == 1:
            max1.append(max.get_ydata()[0])
    for index, max in enumerate(bd2['caps']):
        if index % 2 == 1:
            max2.append(max.get_ydata()[0])
    # print(med1)
    # print(med2)
    # zipping the medians, categories, and data together lets us sort
    # the categories and data by the median values.
    zip1 = zip(med1, max1, catSort1, data1)
    zip2 = zip(med2, max2, catSort2, data2)
    sorted1 = sorted(zip1)
    sorted2 = sorted(zip2)
    med1, max1, catSort1, sortedData1 = [list(tuple) for tuple in zip(*sorted1)]
    med2, max2, catSort2, sortedData2 = [list(tuple) for tuple in zip(*sorted2)]

    ax1.cla()
    ax2.cla()
    ax1.set_xticklabels(catSort1, rotation=45, ha='right')
    ax2.set_xticklabels(catSort2, rotation=45, ha='right')
    ax1.set_xlabel('Relationship')
    ax1.set_ylabel(ylabel)
    ax1.set_title(title1)
    ax2.set_xlabel('Relationship')
    ax2.set_ylabel(ylabel)
    ax2.set_title(title2)

    bd1 = ax1.boxplot(sortedData1)
    bd2 = ax2.boxplot(sortedData2)


def autolabel(rects, ax, n):
    # Attach a text label above each bar in rects
    i = 0
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(n[i]),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')
        i += 1


def barSideBySide(dict1, dict2, ylabel, title1, title2, std=False, mode=False):
    # get averages and standard deviations
    avgs1 = []
    avgs2 = []
    mode1 = []
    mode2 = []
    stds1 = []
    stds2 = []
    for bin in dict1:
        avgs1.append(np.average(dict1[bin]))
        avgs2.append(np.average(dict2[bin]))
        mode1.append(max(set(dict1[bin]), key=dict1[bin].count))
        mode2.append(max(set(dict2[bin]), key=dict2[bin].count))
        stds1.append(np.std(dict1[bin]))
        stds2.append(np.std(dict2[bin]))

    # How many tweets are in each bin?
    n1 = []
    n2 = []
    for r in categories:
        n1.append(len(dict1[r]))
        n2.append(len(dict2[r]))

    # use plt.xticks somehow to rotate x labels 90 degrees
    fig, axes = plt.subplots(ncols=2)
    ax1, ax2 = axes.flatten()
    ax1.set_xlabel('Relationship')
    ax1.set_ylabel(ylabel)
    ax1.set_title(title1)
    ax1.set_xticklabels(categories, rotation=45, ha='right')
    ax2.set_xlabel('Relationship')
    ax2.set_ylabel(ylabel)
    ax2.set_title(title2)
    ax2.set_xticklabels(categories, rotation=45, ha='right')
    if std and not mode:
        rects1 = ax1.bar(categories, avgs1, yerr=stds1, capsize=8)
        rects2 = ax2.bar(categories, avgs2, yerr=stds2, capsize=8)
    elif mode and not std:
        rects1 = ax1.bar(categories, mode1)
        rects2 = ax2.bar(categories, mode2)
        autolabel(rects1, ax1, n1)
        autolabel(rects2, ax2, n2)
    elif mode and std:
        rects1 = ax1.bar(categories, mode1, yerr=stds1, capsize=8)
        rects2 = ax2.bar(categories, mode2, yerr=stds2, capsize=8)
    else:
        rects1 = ax1.bar(categories, avgs1)
        rects2 = ax2.bar(categories, avgs2)
        autolabel(rects1, ax1, n1)
        autolabel(rects2, ax2, n2)
    fig.tight_layout()


def plotSingleData(data, ylabel, title):
    fig, ax = plt.subplots()
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.hist(data)  # , range(len(data)))


def plotTheData():
    dicts = getDicts()

    barSideBySide(dicts[0], dicts[1], 'Replies', 'India Replies', 'US Replies')
    barSideBySide(dicts[2], dicts[3], 'Retweets', 'India Retweets', 'US Retweets')
    barSideBySide(dicts[4], dicts[5], 'Likes', 'India Likes', 'US Likes')

    plt.show()


# plotTheData()
