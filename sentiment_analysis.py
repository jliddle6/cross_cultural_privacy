from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import plot_the_data as ptd
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sc

# NOTES: .polarity_scores(sentence) gives a sentiment_dict that has
# pos, neg, neu, and compound scores
sid = SentimentIntensityAnalyzer()


def analyzeSentiment():
    # Get all the tweets, analyze them all and put that into a dict

    data = ptd.getDicts()
    india_tweets_dict = data[6]
    us_tweets_dict = data[7]

    india_pos = {}
    india_neu = {}
    india_neg = {}
    india_compound = {}
    us_pos = {}
    us_neu = {}
    us_neg = {}
    us_compound = {}
    for r in ptd.categories:
        india_pos[r] = []
        india_neu[r] = []
        india_neg[r] = []
        india_compound[r] = []
        us_pos[r] = []
        us_neg[r] = []
        us_neu[r] = []
        us_compound[r] = []

    for r in ptd.categories:
        for tweet in india_tweets_dict[r]:
            india_pos[r].append(sid.polarity_scores(tweet)['pos']*100)
            india_neu[r].append(sid.polarity_scores(tweet)['neu']*100)
            india_neg[r].append(sid.polarity_scores(tweet)['neg']*100)
            india_compound[r].append(sid.polarity_scores(tweet)['compound']*100)
        for tweet in us_tweets_dict[r]:
            us_pos[r].append(sid.polarity_scores(tweet)['pos']*100)
            us_neu[r].append(sid.polarity_scores(tweet)['neu']*100)
            us_neg[r].append(sid.polarity_scores(tweet)['neg']*100)
            us_compound[r].append(sid.polarity_scores(tweet)['compound']*100)
    return [india_pos, india_neu, india_neg, india_compound, us_pos, us_neu, us_neg, us_compound]


def plotSentiment(bar=True):
    # Plot the average sentiment. and the mode sentiment?

    sentiment = analyzeSentiment()
    india_pos = sentiment[0]
    india_neu = sentiment[1]
    india_neg = sentiment[2]
    india_compound = sentiment[3]
    us_pos = sentiment[4]
    us_neu = sentiment[5]
    us_neg = sentiment[6]
    us_compound = sentiment[7]
    if bar:
        ptd.barSideBySide(india_pos, us_pos,
                          'Positive Sentiment Score', 'India', 'US')
        # ptd.barSideBySide(india_neu, us_neu,
        #                    'Neutral Sentiment Score', 'India', 'US')
        ptd.barSideBySide(india_neg, us_neg,
                          'Negative Sentiment Score', 'India', 'US')
        ptd.barSideBySide(india_compound, us_compound,
                          'Compound Sentiment Score', 'India', 'US')
    else:
        ptd.boxSideBySide(india_pos, us_pos, 'Positive Sentiment Score', 'India', 'US')
        ptd.boxSideBySide(india_neg, us_neg, 'Negative Sentiment Score', 'India', 'US')
        ptd.boxSideBySide(india_compound, us_compound, 'Compound Sentiment Score', 'India', 'US')

    plt.show()


def findLengths():
    # find the average length of each tweet

    data = ptd.getDicts()
    india_tweets_dict = data[6]
    us_tweets_dict = data[7]

    india_length = {}
    us_length = {}
    for r in ptd.categories:
        india_length[r] = []
        us_length[r] = []

    for r in ptd.categories:
        for tweet in india_tweets_dict[r]:
            india_length[r].append(len(tweet))
        for tweet in us_tweets_dict[r]:
            us_length[r].append(len(tweet))

    ptd.barSideBySide(india_length, us_length, 'Average Length', 'India', 'US')
    plt.show()


def mannWhitUTest():
    sentiment = analyzeSentiment()
    india_pos = sentiment[0]
    india_neu = sentiment[1]
    india_neg = sentiment[2]
    india_compound = sentiment[3]
    us_pos = sentiment[4]
    us_neu = sentiment[5]
    us_neg = sentiment[6]
    us_compound = sentiment[7]

    # for i in range(len(ptd.categories)):
    #     print(ptd.categories[i], " positive: ", sc.mannwhitneyu(
    #         india_pos[ptd.categories[i]], us_pos[ptd.categories[i]]))
    #     print(ptd.categories[i], " negative: ", sc.mannwhitneyu(
    #         india_neg[ptd.categories[i]], us_neg[ptd.categories[i]]))
    #     print(ptd.categories[i], " compound: ", sc.mannwhitneyu(
    #         india_compound[ptd.categories[i]], us_compound[ptd.categories[i]]))

    threshold = .001
    print("India:")
    for category1 in ptd.categories:
        for category2 in ptd.categories:
            p_pos = sc.mannwhitneyu(india_pos[category1], india_pos[category2]).pvalue
            p_neg = sc.mannwhitneyu(india_neg[category1], india_neg[category2]).pvalue
            p_com = sc.mannwhitneyu(india_compound[category1], india_compound[category2]).pvalue

            if p_pos < threshold:
                print(category1, " compared to ", category2, " positive: ", p_pos)
            if p_neg < threshold:
                print(category1, " compared to ", category2, " negative: ", p_neg)
            if p_com < threshold:
                print(category1, " compared to ", category2, " compound: ", p_com)

    print("US:")
    for category1 in ptd.categories:
        for category2 in ptd.categories:
            p_pos = sc.mannwhitneyu(us_pos[category1], us_pos[category2]).pvalue
            p_neg = sc.mannwhitneyu(us_neg[category1], us_neg[category2]).pvalue
            p_com = sc.mannwhitneyu(us_compound[category1], us_compound[category2]).pvalue

            if p_pos < threshold:
                print(category1, " compared to ", category2, " positive: ", p_pos)
            if p_neg < threshold:
                print(category1, " compared to ", category2, " negative: ", p_neg)
            if p_com < threshold:
                print(category1, " compared to ", category2, " compound: ", p_com)


def histData():
    # Gives a histogram of each individual set of data
    sentiment = analyzeSentiment()
    india_pos = sentiment[0]
    india_neu = sentiment[1]
    india_neg = sentiment[2]
    india_compound = sentiment[3]
    us_pos = sentiment[4]
    us_neu = sentiment[5]
    us_neg = sentiment[6]
    us_compound = sentiment[7]

    for category in ptd.categories:
        ptd.plotSingleData(india_pos[category],
                           '# Tweets with Given Score', 'Positive India ' + category)
        ptd.plotSingleData(india_neg[category],
                           '# Tweets with Given Score', 'Negative India ' + category)
        ptd.plotSingleData(us_pos[category], '# Tweets with Given Score', 'Positive US ' + category)
        ptd.plotSingleData(us_neg[category], '# Tweets with Given Score', 'Negative US ' + category)

    plt.show()


# histData()
# plotSentiment(False)
mannWhitUTest()
